library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pc is
  port (
    clk          : in  std_logic;
    rst          : in  std_logic;
    wr_en        : in  std_logic;
    jump_en      : in  std_logic;
    branch_en    : in  std_logic;
    branch_delta : in  unsigned (15 downto 0);
    jump_address : in  unsigned (15 downto 0);
    data_out     : out unsigned (15 downto 0)
  );
end entity;

architecture a_pc of pc is

  component register16bits is
    port (
      clk      : in  std_logic;
      rst      : in  std_logic;
      wr_en    : in  std_logic;
      data_in  : in  unsigned (15 downto 0);
      data_out : out unsigned (15 downto 0)
    );
  end component;

  signal register_out, pc_jump : unsigned (15 downto 0);

begin
  reg : register16bits port map (clk => clk, rst => rst, wr_en => wr_en, data_in => pc_jump, 
    data_out => register_out);

  pc_jump <= jump_address when jump_en = '1' else
             register_out + branch_delta + 1 when branch_en = '1' else
             register_out + 1;

  data_out <= register_out;
end architecture;
