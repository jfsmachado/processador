library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pc_rom is
  port (
    clk          : in  std_logic;
    rst          : in  std_logic;
    wr_en        : in  std_logic;
    jump_en      : in  std_logic;
    branch_en    : in  std_logic;
    branch_delta : in  unsigned (15 downto 0);
    jump_address : in  unsigned (15 downto 0);
    pc_out       : out unsigned (15 downto 0);
    rom_out      : out unsigned (14 downto 0)
  );
end entity;

architecture a_pc_rom of pc_rom is

  component pc is
    port (
      clk          : in  std_logic;
      rst          : in  std_logic;
      wr_en        : in  std_logic;
      jump_en      : in  std_logic;
      branch_en    : in  std_logic;
      branch_delta : in  unsigned (15 downto 0);
      jump_address : in  unsigned (15 downto 0);
      data_out     : out unsigned (15 downto 0)
    );
  end component;

  component rom is
    port (
      clk     : in  std_logic;
      address : in  unsigned (15 downto 0);
      data    : out unsigned (14 downto 0)
    );
  end component;

  signal data_pc_out : unsigned (15 downto 0);

begin
  pc0 : pc port map (clk => clk, rst => rst, wr_en => wr_en, jump_en => jump_en, branch_en => branch_en,
  branch_delta => branch_delta, jump_address => jump_address, data_out => data_pc_out);

  rom0 : rom port map (clk => clk, address => data_pc_out, data => rom_out);

  pc_out <= data_pc_out;
end architecture;
