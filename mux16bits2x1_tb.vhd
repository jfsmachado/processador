library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux16bits2x1_tb is
end entity;

architecture a_mux16bits2x1_tb of mux16bits2x1_tb is
  component mux16bits2x1
    port (
      sel_data  : in  std_logic;
      data_in_0 : in  unsigned (15 downto 0);
      data_in_1 : in  unsigned (15 downto 0);
      data_out  : out unsigned (15 downto 0)
    );
  end component;

  signal sel_data : std_logic;
  signal data_in_0, data_in_1, data_out : unsigned (15 downto 0);

begin
  uut: mux16bits2x1 port map (
    sel_data  => sel_data,
    data_in_0 => data_in_0,
    data_in_1 => data_in_1,
    data_out  => data_out
  );

  process begin
    data_in_0 <= "0000000000000000";
    data_in_1 <= "0000000000000001";
    wait for 50 ns;
    sel_data <= '0';
    wait for 50 ns;
    sel_data <= '1';
    wait for 50 ns;
    wait;
  end process;
end architecture;
