library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity controlunit is
  port (
    clk             : in  std_logic;
    rst             : in  std_logic;
    instruction     : in  unsigned (14 downto 0);
    sr_flags        : in  unsigned (5  downto 0);
    sr_flags_wr_en  : out unsigned (5  downto 0);
    state           : out std_logic;
    pc_wr_en        : out std_logic;
    pc_jump_en      : out std_logic;
    pc_jump_address : out unsigned (15 downto 0);
    alu_src         : out std_logic;
    alu_sel_op      : out unsigned (1  downto 0);
    rb_wr_en        : out std_logic;
    rb_wr_reg       : out unsigned (2  downto 0);
    rb_rd_reg_a     : out unsigned (2  downto 0);
    rb_rd_reg_b     : out unsigned (2  downto 0);
    alu_const       : out unsigned (15 downto 0);
    pc_branch_en    : out std_logic;
    pc_branch_delta : out unsigned (15 downto 0);
    rb_wr_src       : out std_logic;
    ram_wr_en       : out std_logic
  );
end entity;

architecture a_controlunit of controlunit is

  component statemachine
    port (
      clk   : in  std_logic;
      rst   : in  std_logic;
      state : out std_logic
    );
  end component;

  signal opcode : unsigned (3 downto 0);
  signal state_s, pc_jump_en_s, pc_branch_en_s: std_logic;

begin
  fsm : statemachine port map (clk => clk, rst => rst, state => state_s);

  opcode <= instruction (14 downto 11);

  rb_wr_src <= '1' when opcode = "1000" and state_s = '1' else
               '0';

  ram_wr_en <= '1' when opcode = "1001" and state_s = '1' else
               '0';

  sr_flags_wr_en <= "111111" when opcode = "0001" and state_s = '1' else -- CP
                    "111111" when opcode = "0010" and state_s = '1' else -- CPI
                    "111111" when opcode = "0011" and state_s = '1' else -- ADD
                    "111111" when opcode = "0101" and state_s = '1' else -- SUBI
                    "111111" when opcode = "0110" and state_s = '1' else -- SUBI
                    "000000";

  pc_branch_en_s <= '1' when opcode = "1100" and state_s = '1' and sr_flags(1) = '1' else -- BRLT
                    '0';

  pc_branch_en <= pc_branch_en_s;

  pc_branch_delta <= (15 downto 11 => instruction(10)) & instruction (10 downto 0) when pc_branch_en_s = '1' and state_s = '1' else
                     "0000000000000000";

  pc_jump_en_s <= '1' when opcode = "1010" and state_s = '1' else -- JMP  
                  '0';

  pc_jump_en <= pc_jump_en_s;

  pc_jump_address <= "00000" & instruction (10 downto 0) when pc_jump_en_s = '1' and state_s = '1' else
                     "0000000000000000";

  pc_wr_en <= '1' when state_s = '1' or pc_jump_en_s = '1' or pc_branch_en_s = '1' else
              '0';

  state <= state_s;

  alu_src <= '1' when (opcode = "1110" or opcode = "0101" or opcode = "0010") and state_s = '1' else -- LDI, SUBI ou CPI
             '0';

  alu_sel_op <= "00" when opcode = "0011" and state_s = '1' else -- ADD
                "01" when (opcode = "0110" or opcode = "0101") and state_s = '1' else --SUB ou SUBI
                "10" when (opcode = "1110" or opcode = "1011" or opcode = "1001" or opcode = "1000") and state_s = '1' else -- LDI, MOV, STS ou LDS
                "11" when (opcode = "0001" or opcode = "0010") and state_s = '1' else -- CP ou CPI 
                "11";

  rb_wr_en <= '1' when (opcode = "1110" or opcode = "1011" or opcode = "0011" or opcode = "0110" or opcode = "0101" or opcode = "1000") and state_s = '1' else -- LDI, MOV, ADD, SUB, SUBI, LDS 
              '0';

  rb_wr_reg <= instruction (5 downto 3) when state_s = '1' else
               "000";

  rb_rd_reg_a <= instruction (5 downto 3) when state_s = '1' else
               "000";

  rb_rd_reg_b <= instruction (2 downto 0) when state_s = '1' else
               "000";

  alu_const <= (15 downto 8 => instruction(10)) & instruction (10 downto 6) & instruction (2 downto 0) when state_s = '1' else
               "0000000000000000";

end architecture;
