library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity processador_tb is
end entity;

architecture a_processador_tb of processador_tb is
  component processador
    port (
      clk             : in  std_logic;
      rst             : in  std_logic;
      state           : out std_logic;
      pc              : out unsigned (15 downto 0);
      instruction     : out unsigned (14 downto 0);
      data_reg_a      : out unsigned (15 downto 0);
      data_reg_b      : out unsigned (15 downto 0);
      data_alu_out    : out unsigned (15 downto 0);
      flags           : out unsigned (5  downto 0)
    );
  end component;

  signal clk, rst, state : std_logic;
  signal flags : unsigned (5 downto 0);
  signal instruction : unsigned (14 downto 0);
  signal pc, data_reg_a, data_reg_b, data_alu_out : unsigned (15 downto 0);

begin
  uut: processador port map (
    clk          => clk,
    rst          => rst,
    state        => state,
    pc           => pc,
    instruction  => instruction,
    data_reg_a   => data_reg_a,
    data_reg_b   => data_reg_b,
    data_alu_out => data_alu_out,
    flags        => flags
  );

  process begin
    clk <= '0';
    wait for 50 ns;
    clk <= '1';
    wait for 50 ns;
  end process;

  process begin
    rst <= '1';
    wait for 100 ns;
    rst <= '0';
    wait;
  end process;
end architecture;
