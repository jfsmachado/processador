library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pc_rom_tb is
end entity;

architecture a_pc_rom_tb of pc_rom_tb is
  component pc_rom
    port (
      clk          : in  std_logic;
      rst          : in  std_logic;
      wr_en        : in  std_logic;
      jump_en      : in  std_logic;
      branch_en    : in  std_logic;
      branch_delta : in  unsigned (15 downto 0);
      jump_address : in  unsigned (15 downto 0);
      pc_out       : out unsigned (15 downto 0);
      rom_out      : out unsigned (14 downto 0)
    );
  end component;

  signal clk, rst, wr_en, jump_en, branch_en : std_logic;
  signal branch_delta, jump_address, pc_out : unsigned (15 downto 0);
  signal rom_out : unsigned (14 downto 0);

begin
  uut: pc_rom port map (
    clk          => clk,
    rst          => rst,
    wr_en        => wr_en,
    jump_en      => jump_en,
    branch_en    => branch_en,
    branch_delta => branch_delta,
    jump_address => jump_address,
    pc_out       => pc_out,
    rom_out      => rom_out
  );

  process begin
    clk <= '0';
    wait for 50 ns;
    clk <= '1';
    wait for 50 ns;
  end process;

  process begin
    rst <= '1';
    wait for 100 ns;
    rst <= '0';
    wait;
  end process;

  process begin
    wr_en <= '1';
    jump_en <= '0';
    branch_en <= '0';
    branch_delta <= "0000000000000000";
    jump_address <= "0000000000000000";
    wait for 100 ns;
    branch_en <= '1';
    branch_delta <= "0000000000000011";
    wait for 100 ns;
    branch_en <= '0';
    wait for 100 ns;
    branch_en <= '1';
    branch_delta <= "1111111111111110";
    wait for 100 ns;
    branch_en <= '0';
    wait for 1000 ns;
    jump_en <= '1';
    jump_address <= "0000000000000000";
    wait for 100 ns;
    jump_en <= '0';
    wait for 500 ns;
    jump_en <= '1';
    jump_address <= "0000000000001000";
    wait for 100 ns;
    jump_en <= '0';
    wait for 100 ns;
    wait;
  end process;

end architecture;
