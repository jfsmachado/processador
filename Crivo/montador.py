import os
import sys

def splitInstruction(line, assemblyLine):
    instruction = {"line" : 0, "function" : "", "parameter1" : "", "parameter2" : "", "opcode" : ""}
    instruction["line"] = line
    instruction["function"] = assemblyLine.partition(" ")[0]
    instruction["parameter1"] = assemblyLine.partition(" ")[2].partition(",")[0]
    instruction["parameter2"] = assemblyLine.partition(" ")[2].partition(",")[2]
    return instruction

def getAssemblyFromFile(file):
    assembly = []
    n = 0
    for line in file:
        assemblyLine = line.partition("\n")[0]
        assembly.append(splitInstruction(n, assemblyLine))
        n += 1
    file.close() 
    return assembly

def getRegisterBinary(register):
    binary = ""
    n = int(register[1])
    pot = 4
    while pot >= 1:
        if n >= pot:
            binary += "1"
            n -= pot
            pot /= 2
        else:
            binary += "0"
            pot /= 2
    return binary

def getConstantBinary(constant):
    binary = ""
    n = int(constant)
    if n >= 0:
        binary += "0"
    else:
        binary += "1"
        n = 128 + n
    pot = 64
    while pot >= 1:
        if n >= pot:
            binary += "1"
            n -= pot
            pot /= 2
        else:
            binary += "0"
            pot /= 2
    return binary

def getJumpBinary(constant):
    binary = ""
    n = int(constant)
    if n >= 0:
        binary += "0"
    else:
        binary += "1"
        n = 1024 + n
    pot = 512
    while pot >= 1:
        if n >= pot:
            binary += "1"
            n -= pot
            pot /= 2
        else:
            binary += "0"
            pot /= 2
    return binary

def getOpcode(assemblyLine):
    opcode = ""
    if assemblyLine["function"] == "LDI":
        opcode += "1110"
        opcode += getConstantBinary(assemblyLine["parameter2"])[:5]
        opcode += getRegisterBinary(assemblyLine["parameter1"])
        opcode += getConstantBinary(assemblyLine["parameter2"])[-3:]
    elif assemblyLine["function"] == "MOV":
        opcode += "101100000"
        opcode += getRegisterBinary(assemblyLine["parameter1"])
        opcode += getRegisterBinary(assemblyLine["parameter2"])
    elif assemblyLine["function"] == "ADD":
        opcode += "001100000"
        opcode += getRegisterBinary(assemblyLine["parameter1"])
        opcode += getRegisterBinary(assemblyLine["parameter2"])
    elif assemblyLine["function"] == "SUB":
        opcode += "011000000"
        opcode += getRegisterBinary(assemblyLine["parameter1"])
        opcode += getRegisterBinary(assemblyLine["parameter2"])
    elif assemblyLine["function"] == "SUBI":
        opcode += "0101"
        opcode += getConstantBinary(assemblyLine["parameter2"])[:5]
        opcode += getRegisterBinary(assemblyLine["parameter1"])
        opcode += getConstantBinary(assemblyLine["parameter2"])[-3:]
    elif assemblyLine["function"] == "CP":
        opcode += "000100000"
        opcode += getRegisterBinary(assemblyLine["parameter1"])
        opcode += getRegisterBinary(assemblyLine["parameter2"])
    elif assemblyLine["function"] == "CPI":
        opcode += "0010"
        opcode += getConstantBinary(assemblyLine["parameter2"])[:5]
        opcode += getRegisterBinary(assemblyLine["parameter1"])
        opcode += getConstantBinary(assemblyLine["parameter2"])[-3:]
    elif assemblyLine["function"] == "BRLT":
        opcode += "1100"
        opcode += getJumpBinary(assemblyLine["parameter1"])
    elif assemblyLine["function"] == "JMP":
        opcode += "1010"
        opcode += getJumpBinary(assemblyLine["parameter1"])
    elif assemblyLine["function"] == "LD":
        opcode += "100000000"
        opcode += getRegisterBinary(assemblyLine["parameter1"])
        opcode += getRegisterBinary(assemblyLine["parameter2"])
    elif assemblyLine["function"] == "ST":
        opcode += "100100000"
        opcode += getRegisterBinary(assemblyLine["parameter1"])
        opcode += getRegisterBinary(assemblyLine["parameter2"])
    else:
        opcode += "000000000000000"
    return opcode

def translateAssembly(assembly):
    for a in assembly:
        a["opcode"] = getOpcode(a)
    return assembly

def outputFile(assembly):
    fileOut = open(os.path.join(sys.path[0], "ROM.txt"),"w")
    n = 0
    for a in assembly:
        fileOut.write("{:2} => \"{}\", -- {:4} {}".format(n, a["opcode"], a["function"], a["parameter1"]))
        if(a["parameter2"] != ""):
            fileOut.write(",{}".format(a["parameter2"]))
        fileOut.write("\n") 
        n += 1
    fileOut.close()

file = open(os.path.join(sys.path[0], "Assembly.txt"), "r")
assembly = getAssemblyFromFile(file)
assembly = translateAssembly(assembly)
outputFile(assembly)
