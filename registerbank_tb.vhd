library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity registerbank_tb is
end entity;

architecture a_registerbank_tb of registerbank_tb is
  component registerbank
    port (
      clk        : in  std_logic;
      rst        : in  std_logic;
      wr_en      : in  std_logic;
      wr_reg     : in  unsigned (2  downto 0);
      rd_reg_a   : in  unsigned (2  downto 0);
      rd_reg_b   : in  unsigned (2  downto 0);
      wr_data    : in  unsigned (15 downto 0);
      data_reg_a : out unsigned (15 downto 0);
      data_reg_b : out unsigned (15 downto 0)
    );
  end component;

  signal clk, rst, wr_en : std_logic;
  signal wr_reg, rd_reg_a, rd_reg_b : unsigned (2  downto 0);
  signal wr_data, data_reg_a, data_reg_b : unsigned (15 downto 0);

begin
  uut: registerbank port map (
    clk        => clk,
    rst        => rst,
    wr_en      => wr_en,
    wr_reg     => wr_reg,
    rd_reg_a   => rd_reg_a,
    rd_reg_b   => rd_reg_b,
    wr_data    => wr_data,
    data_reg_a => data_reg_a,
    data_reg_b => data_reg_b
  );

  process begin
    clk <= '0';
    wait for 50 ns;
    clk <= '1';
    wait for 50 ns;
  end process;

  process begin
    rst <= '1';
    wait for 100 ns;
    rst <= '0';
    wait for 1100 ns;
    rst <= '1';
    wait;
  end process;

  process begin
    wait for 100 ns;
    wr_en <= '0';
    wr_reg <= "000";
    rd_reg_a <= "000";
    rd_reg_b <= "001";
    wr_data <= "1010101010101010";
    wait for 200 ns;
    wr_en <= '1';
    wr_data <= "1111111100000000";
    wait for 100 ns;
    wr_reg <= "001";
    wr_data <= "1111111100000001";
    wait for 100 ns;
    wr_reg <= "010";
    wr_data <= "1111111100000010";
    rd_reg_a <= "001";
    rd_reg_b <= "010";
    wait for 100 ns;
    wr_reg <= "011";
    wr_data <= "1111111100000011";
    rd_reg_a <= "010";
    rd_reg_b <= "011";
    wait for 100 ns;
    wr_reg <= "100";
    wr_data <= "1111111100000100";
    rd_reg_a <= "011";
    rd_reg_b <= "100";
    wait for 100 ns;
    wr_reg <= "101";
    wr_data <= "1111111100000101";
    rd_reg_a <= "100";
    rd_reg_b <= "101";
    wait for 100 ns;
    wr_reg <= "110";
    wr_data <= "1111111100000110";
    rd_reg_a <= "101";
    rd_reg_b <= "110";
    wait for 100 ns;
    wr_reg <= "111";
    wr_data <= "1111111100000111";
    rd_reg_a <= "110";
    rd_reg_b <= "111";
    wait for 100 ns;
    wait;
  end process;
end architecture;
