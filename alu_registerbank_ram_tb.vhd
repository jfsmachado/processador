library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu_registerbank_ram_tb is
end entity;

architecture a_alu_registerbank_ram_tb of alu_registerbank_ram_tb is
  component alu_registerbank_ram
    port (
      clk             : in  std_logic;
      rst             : in  std_logic;
      alu_src         : in  std_logic;
      wr_en           : in  std_logic;
      sel_op          : in  unsigned (1  downto 0);
      wr_reg          : in  unsigned (2  downto 0);
      rd_reg_a        : in  unsigned (2  downto 0);
      rd_reg_b        : in  unsigned (2  downto 0);
      const           : in  unsigned (15 downto 0);
      flags_wr_en     : in  unsigned (5  downto 0);
      rb_wr_src       : in  std_logic;
      ram_wr_en       : in  std_logic;
      data_reg_a      : out unsigned (15 downto 0);
      data_reg_b      : out unsigned (15 downto 0);
      data_out        : out unsigned (15 downto 0);
      flags           : out unsigned (5  downto 0)
    );
  end component;

  signal alu_src, clk, rst, wr_en, rb_wr_src, ram_wr_en : std_logic;
  signal sel_op : unsigned (1 downto 0);
  signal wr_reg, rd_reg_a, rd_reg_b : unsigned (2 downto 0);
  signal flags_wr_en, flags : unsigned (5 downto 0);
  signal const, data_reg_a, data_reg_b, data_out : unsigned (15 downto 0);

begin
  uut: alu_registerbank_ram port map (
    alu_src     => alu_src,
    clk         => clk,
    rst         => rst,
    wr_en       => wr_en,
    sel_op      => sel_op,
    wr_reg      => wr_reg,
    rd_reg_a    => rd_reg_a,
    rd_reg_b    => rd_reg_b,
    const       => const,
    data_reg_a  => data_reg_a,
    data_reg_b  => data_reg_b,
    data_out    => data_out,
    flags_wr_en => flags_wr_en,
    flags       => flags,
    rb_wr_src   => rb_wr_src,
    ram_wr_en   => ram_wr_en
  );

  process begin
    clk <= '0';
    wait for 50 ns;
    clk <= '1';
    wait for 50 ns;
  end process;

  process begin
    rst <= '1';
    wait for 100 ns;
    rst <= '0';
    wait;
  end process;

  process begin
    wr_en <= '1';
    flags_wr_en <= "111111";
    sel_op <= "00";
    wr_reg <= "000";
    rd_reg_a <= "000";
    rd_reg_b <= "000";
    const <= "0000000000000000";
    alu_src <= '0';
    rb_wr_src <= '0';
    ram_wr_en <= '0';
    wait for 100 ns;
    -- LDI R0,0
    sel_op <= "10";
    const <= "0000000000000000";
    alu_src <= '1';
    wait for 100 ns;
    -- R7,7
    wr_reg <= "111";
    const <= "0000000000000111";
    wait for 100 ns;
    -- STS R7,(R0)
    wr_en <= '0';
    rd_reg_a <= "111";
    rd_reg_b <= "000";
    alu_src <= '0';
    ram_wr_en <= '1';
    wait for 100 ns;
    -- LDS R6,(R0)
    wr_en <= '1';
    wr_reg <= "110";
    rb_wr_src <= '1';
    ram_wr_en <= '0';
    wait for 100 ns;
    wait;
  end process;
end architecture;
