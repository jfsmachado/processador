library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity statemachine is
  port (
    clk   : in  std_logic;
    rst   : in  std_logic;
    state : out std_logic
  );
end entity;

architecture a_statemachine of statemachine is

  signal s : std_logic;

begin
  process(clk, rst)
  begin
    if rst = '1' then
      s <= '0';
    elsif rising_edge(clk) then
        s <= not s;
    end if;
  end process;
  state <= s;
end architecture;
