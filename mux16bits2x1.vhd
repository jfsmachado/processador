library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux16bits2x1 is
  port (
    sel_data  : in  std_logic;
    data_in_0 : in  unsigned (15 downto 0);
    data_in_1 : in  unsigned (15 downto 0);
    data_out  : out unsigned (15 downto 0)
  );
end entity;

architecture a_mux16bits2x1 of mux16bits2x1 is
begin
  data_out <= data_in_0 when sel_data = '0' else
              data_in_1 when sel_data = '1' else
              "0000000000000000";
end architecture;
