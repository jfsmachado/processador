library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity statemachine_tb is
end entity;

architecture a_statemachine_tb of statemachine_tb is
  component statemachine
    port (
      clk   : in  std_logic;
      rst   : in  std_logic;
      state : out std_logic
    );
  end component;

  signal clk, rst, state : std_logic;

begin
  uut: statemachine port map (
    clk   => clk,
    rst   => rst,
    state => state
  );

  process begin
    clk <= '0';
    wait for 50 ns;
    clk <= '1';
    wait for 50 ns;
  end process;

  process begin
    rst <= '1';
    wait for 100 ns;
    rst <= '0';
    wait for 500 ns;
    rst <= '1';
    wait for 100 ns;
    rst <= '0';
    wait;
  end process;
end architecture;
