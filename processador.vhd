library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity processador is
  port (
    clk             : in  std_logic;
    rst             : in  std_logic;
    state           : out std_logic;
    pc              : out unsigned (15 downto 0);
    instruction     : out unsigned (14 downto 0);
    data_reg_a      : out unsigned (15 downto 0);
    data_reg_b      : out unsigned (15 downto 0);
    data_alu_out    : out unsigned (15 downto 0);
    flags           : out unsigned (5  downto 0)
  );
end entity;

architecture a_processador of processador is

  component controlunit is
    port (
      clk             : in  std_logic;
      rst             : in  std_logic;
      instruction     : in  unsigned (14 downto 0);
      sr_flags        : in  unsigned (5  downto 0);
      sr_flags_wr_en  : out unsigned (5  downto 0);
      state           : out std_logic;
      pc_wr_en        : out std_logic;
      pc_jump_en      : out std_logic;
      pc_jump_address : out unsigned (15 downto 0);
      alu_src         : out std_logic;
      alu_sel_op      : out unsigned (1  downto 0);
      rb_wr_en        : out std_logic;
      rb_wr_reg       : out unsigned (2  downto 0);
      rb_rd_reg_a     : out unsigned (2  downto 0);
      rb_rd_reg_b     : out unsigned (2  downto 0);
      alu_const       : out unsigned (15 downto 0);
      pc_branch_en    : out std_logic;
      pc_branch_delta : out unsigned (15 downto 0);
      rb_wr_src       : out std_logic;
      ram_wr_en       : out std_logic
    );
  end component;

  component pc_rom is
    port (
      clk          : in  std_logic;
      rst          : in  std_logic;
      wr_en        : in  std_logic;
      jump_en      : in  std_logic;
      branch_en    : in  std_logic;
      branch_delta : in  unsigned (15 downto 0);
      jump_address : in  unsigned (15 downto 0);
      pc_out       : out unsigned (15 downto 0);
      rom_out      : out unsigned (14 downto 0)
    );
  end component;

  component alu_registerbank_ram is
    port (
      clk             : in  std_logic;
      rst             : in  std_logic;
      alu_src         : in  std_logic;
      wr_en           : in  std_logic;
      sel_op          : in  unsigned (1  downto 0);
      wr_reg          : in  unsigned (2  downto 0);
      rd_reg_a        : in  unsigned (2  downto 0);
      rd_reg_b        : in  unsigned (2  downto 0);
      const           : in  unsigned (15 downto 0);
      flags_wr_en     : in  unsigned (5  downto 0);
      rb_wr_src       : in  std_logic;
      ram_wr_en       : in  std_logic;
      data_reg_a      : out unsigned (15 downto 0);
      data_reg_b      : out unsigned (15 downto 0);
      data_out        : out unsigned (15 downto 0);
      flags           : out unsigned (5  downto 0)
    );
  end component;

  signal pc_wr_en_s, pc_jump_en_s, pc_branch_en_s, alu_src_s, rb_wr_en_s, rb_wr_src_s, ram_wr_en_s : std_logic;
  signal alu_sel_op_s : unsigned (1 downto 0);
  signal rb_wr_reg_s, rb_rd_reg_a_s, rb_rd_reg_b_s : unsigned (2 downto 0);
  signal flags_wr_en_s, flags_s : unsigned (5 downto 0);
  signal instruction_s : unsigned (14 downto 0);
  signal pc_jump_address_s, pc_branch_delta_s, alu_const_s : unsigned (15 downto 0);

begin
  controlunit0 : controlunit port map (clk => clk, rst => rst, instruction => instruction_s, sr_flags => flags_s,
    sr_flags_wr_en => flags_wr_en_s, state => state, pc_wr_en => pc_wr_en_s, pc_jump_en => pc_jump_en_s, 
    pc_jump_address => pc_jump_address_s, alu_src => alu_src_s, alu_sel_op => alu_sel_op_s, 
    rb_wr_en => rb_wr_en_s, rb_wr_reg => rb_wr_reg_s, rb_rd_reg_a => rb_rd_reg_a_s, rb_rd_reg_b => rb_rd_reg_b_s,
    alu_const => alu_const_s, pc_branch_en => pc_branch_en_s, pc_branch_delta => pc_branch_delta_s,
    rb_wr_src => rb_wr_src_s, ram_wr_en => ram_wr_en_s);

  pc_rom0 : pc_rom port map (clk => clk, rst => rst, wr_en => pc_wr_en_s, branch_en => pc_branch_en_s,
    branch_delta => pc_branch_delta_s, jump_en => pc_jump_en_s, jump_address => pc_jump_address_s, 
    pc_out => pc, rom_out => instruction_s);

  alu_rb_ram0 : alu_registerbank_ram port map (clk => clk, rst => rst, alu_src => alu_src_s, wr_en => rb_wr_en_s,
    sel_op => alu_sel_op_s, wr_reg => rb_wr_reg_s, rd_reg_a => rb_rd_reg_a_s, rd_reg_b => rb_rd_reg_b_s,
    const => alu_const_s, flags_wr_en => flags_wr_en_s, data_reg_a => data_reg_a, data_reg_b => data_reg_b, 
    data_out => data_alu_out, flags => flags_s, rb_wr_src => rb_wr_src_s, ram_wr_en => ram_wr_en_s);

  instruction <= instruction_s;

  flags <= flags_s;
end architecture;
