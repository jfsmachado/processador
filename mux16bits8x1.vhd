library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux16bits8x1 is
  port (
    sel_data  : in  unsigned (2  downto 0);
    data_in_0 : in  unsigned (15 downto 0);
    data_in_1 : in  unsigned (15 downto 0);
    data_in_2 : in  unsigned (15 downto 0);
    data_in_3 : in  unsigned (15 downto 0);
    data_in_4 : in  unsigned (15 downto 0);
    data_in_5 : in  unsigned (15 downto 0);
    data_in_6 : in  unsigned (15 downto 0);
    data_in_7 : in  unsigned (15 downto 0);
    data_out  : out unsigned (15 downto 0)
  );
end entity;

architecture a_mux16bits8x1 of mux16bits8x1 is
begin
  data_out <= data_in_0 when sel_data = "000" else
              data_in_1 when sel_data = "001" else
              data_in_2 when sel_data = "010" else
              data_in_3 when sel_data = "011" else
              data_in_4 when sel_data = "100" else
              data_in_5 when sel_data = "101" else
              data_in_6 when sel_data = "110" else
              data_in_7 when sel_data = "111" else
              "0000000000000000";
end architecture;
