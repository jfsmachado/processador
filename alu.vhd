library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu is
  port (
    sel_op          : in  unsigned (1  downto 0);
    data_in_a       : in  unsigned (15 downto 0);
    data_in_b       : in  unsigned (15 downto 0);
    data_out        : out unsigned (15 downto 0);
    flags           : out unsigned (5  downto 0)
  );
end entity;

architecture a_alu of alu is

  signal data_out_aux : unsigned (15 downto 0);
  signal carry_flag, zero_flag, negative_flag, overflow_flag, signed_flag, half_carry_flag : std_logic;
  signal negative, overflow : std_logic;

begin
  data_out_aux <= data_in_a + data_in_b when sel_op = "00" else -- ADD
                  data_in_a - data_in_b when sel_op = "01" else -- SUB
                  data_in_b             when sel_op = "10" else -- MOV, LDI, STS
                  data_in_a - data_in_b when sel_op = "11" else -- CP
                  "0000000000000000";

  carry_flag <= ((data_in_a(15) and data_in_b(15)) or
                 (data_in_a(15) and not data_out_aux(15)) or
                 (data_in_b(15) and not data_out_aux(15))) when sel_op = "00" else
                ((not data_in_a(15) and data_in_b(15)) or
                 (data_in_b(15) and data_out_aux(15)) or
                 (not data_in_a(15) and data_out_aux(15))) when (sel_op = "01" or sel_op = "11") else
                '0';

  zero_flag <= '1' when data_out_aux = "0000000000000000" and (sel_op = "00" or sel_op = "01" or sel_op = "11") else
               '0';

  negative <= data_out_aux(15) when (sel_op = "00" or sel_op = "01" or sel_op = "11") else
              '0';

  negative_flag <= negative;

  overflow <= ((data_in_a(15) and data_in_b(15) and not data_out_aux(15)) or
               (not data_in_a(15) and not data_in_b(15) and data_out_aux(15))) when sel_op = "00" else
              ((data_in_a(15) and not data_in_b(15) and not data_out_aux(15)) or
               (not data_in_a(15) and data_in_b(15) and data_out_aux(15))) when (sel_op = "01" or sel_op = "11") else
              '0';

  overflow_flag <= overflow when (sel_op = "00" or sel_op = "01" or sel_op = "11") else
              '0';

  signed_flag <= negative xor overflow when (sel_op = "00" or sel_op = "01" or sel_op = "11") else
              '0';

  half_carry_flag <= ((data_in_a(7) and data_in_b(7)) or
                 (data_in_a(7) and not data_out_aux(7)) or
                 (data_in_b(7) and not data_out_aux(7))) when sel_op = "00" else
                ((not data_in_a(7) and data_in_b(7)) or
                 (data_in_b(7) and data_out_aux(7)) or
                 (not data_in_a(7) and data_out_aux(7))) when (sel_op = "01" or sel_op = "11") else
                '0';

  data_out <= data_out_aux;

  flags <= carry_flag & zero_flag & negative_flag & overflow_flag & signed_flag & half_carry_flag;
end architecture;
