library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu_tb is
end entity;

architecture a_alu_tb of alu_tb is
  component alu
    port (
      sel_op          : in  unsigned (1  downto 0);
      data_in_a       : in  unsigned (15 downto 0);
      data_in_b       : in  unsigned (15 downto 0);
      data_out        : out unsigned (15 downto 0);
      flags           : out unsigned (5  downto 0)
    );
  end component;

  signal sel_op : unsigned (1  downto 0);
  signal data_in_a, data_in_b, data_out : unsigned (15 downto 0);
  signal flags : unsigned (5 downto 0);

begin
  uut: alu port map (
    sel_op    => sel_op,
    data_in_a => data_in_a,
    data_in_b => data_in_b,
    data_out  => data_out,
    flags     => flags
  );

  process begin
  -- ADD TEST --
    sel_op <= "00";
    data_in_a <= "1000000000000000";
    data_in_b <= "1000000000000000";
    wait for 50 ns;
    data_in_a <= "1111111111111111";
    data_in_b <= "1111111111111111";
    wait for 50 ns;
    data_in_a <= "1111111111111111";
    data_in_b <= "0000000000000001";
    wait for 50 ns;
    data_in_a <= "1111111111111111";
    data_in_b <= "1000000000000001";
    wait for 50 ns;
    data_in_a <= "0000000010000000";
    data_in_b <= "0000000010000000";
    wait for 50 ns;
  -- SUB TEST --
    sel_op <= "01";
    data_in_a <= "1000000000000000";
    data_in_b <= "1000000000000000";
    wait for 50 ns;
    data_in_a <= "0000000000000000";
    data_in_b <= "1000000000000000";
    wait for 50 ns;
    data_in_a <= "0111111111111111";
    data_in_b <= "1111111111111111";
    wait for 50 ns;
    data_in_a <= "0000000000000000";
    data_in_b <= "0000000010000000";
    wait for 50 ns;
    -- MOV TEST --
    sel_op <= "10";
    data_in_a <= "1111111111111111";
    data_in_b <= "0000000000000000";
    wait for 50 ns;
    data_in_a <= "0000000000000000";
    data_in_b <= "1111111111111111";
    wait for 50 ns;
    data_in_a <= "1000000000000000";
    data_in_b <= "1000000000000000";
    wait for 50 ns;
  -- CP TEST --
    sel_op <= "11";
    data_in_a <= "1111111111111111";
    data_in_b <= "0000000000000000";
    wait for 50 ns;
    data_in_a <= "0000000000000000";
    data_in_b <= "1111111111111111";
    wait for 50 ns;
    data_in_a <= "1000000000000000";
    data_in_b <= "1000000000000000";
    wait for 50 ns;
    data_in_a <= "0100000000000000";
    data_in_b <= "0100000000000000";
    wait for 50 ns;
    wait;
  end process;
end architecture;
