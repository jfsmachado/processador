library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rom is
  port (
    clk     : in  std_logic;
    address : in  unsigned (15 downto 0);
    data    : out unsigned (14 downto 0)
  );
end entity;

architecture a_rom of rom is
  type mem is array (0 to 65535) of unsigned (14 downto 0);
  constant rom_data : mem := (
    0 => "111001111000111", -- LDI  R0,127
    1 => "111000000001001", -- LDI  R1,1
    2 => "100100000000000", -- ST   R0,R0
    3 => "011000000000001", -- SUB  R0,R1
    4 => "000100000001000", -- CP   R1,R0
    5 => "110011111111100", -- BRLT -4
    6 => "111000000001010", -- LDI  R1,2
    7 => "111001111011111", -- LDI  R3,127
    8 => "111000000100000", -- LDI  R4,0
    9 => "111000000101001", -- LDI  R5,1
   10 => "101100000000001", -- MOV  R0,R1
   11 => "001100000000001", -- ADD  R0,R1
   12 => "000100000011000", -- CP   R3,R0
   13 => "110000000000010", -- BRLT 2
   14 => "100100000100000", -- ST   R4,R0
   15 => "101000000001011", -- JMP  11
   16 => "001100000001101", -- ADD  R1,R5
   17 => "000100000011001", -- CP   R3,R1
   18 => "110000000000100", -- BRLT 4
   19 => "100000000110001", -- LD   R6,R1
   20 => "000100000110101", -- CP   R6,R5
   21 => "110011111111010", -- BRLT -6
   22 => "101100000010001", -- MOV  R2,R1
   23 => "000100000011001", -- CP   R3,R1
   24 => "110000000000001", -- BRLT 1
   25 => "101000000001010", -- JMP  10
    others => (others => '0')
  );
begin
  process(clk)
  begin
    if rising_edge(clk) then
      data <= rom_data(to_integer(address));
    end if;
  end process;
end architecture;
