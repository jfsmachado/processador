library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu_registerbank_ram is
  port (
    clk             : in  std_logic;
    rst             : in  std_logic;
    alu_src         : in  std_logic;
    wr_en           : in  std_logic;
    sel_op          : in  unsigned (1  downto 0);
    wr_reg          : in  unsigned (2  downto 0);
    rd_reg_a        : in  unsigned (2  downto 0);
    rd_reg_b        : in  unsigned (2  downto 0);
    const           : in  unsigned (15 downto 0);
    flags_wr_en     : in  unsigned (5  downto 0);
    rb_wr_src       : in  std_logic;
    ram_wr_en       : in  std_logic;
    data_reg_a      : out unsigned (15 downto 0);
    data_reg_b      : out unsigned (15 downto 0);
    data_out        : out unsigned (15 downto 0);
    flags           : out unsigned (5  downto 0)
  );
end entity;

architecture a_alu_registerbank_ram of alu_registerbank_ram is

  component mux16bits2x1 is
    port (
      sel_data  : in  std_logic;
      data_in_0 : in  unsigned (15 downto 0);
      data_in_1 : in  unsigned (15 downto 0);
      data_out  : out unsigned (15 downto 0)
    );
  end component;

  component alu is
    port (
      sel_op          : in  unsigned (1  downto 0);
      data_in_a       : in  unsigned (15 downto 0);
      data_in_b       : in  unsigned (15 downto 0);
      data_out        : out unsigned (15 downto 0);
      flags           : out unsigned (5  downto 0)
    );
  end component;

  component registerbank is
    port (
      clk        : in  std_logic;
      rst        : in  std_logic;
      wr_en      : in  std_logic;
      wr_reg     : in  unsigned (2  downto 0);
      rd_reg_a   : in  unsigned (2  downto 0);
      rd_reg_b   : in  unsigned (2  downto 0);
      wr_data    : in  unsigned (15 downto 0);
      data_reg_a : out unsigned (15 downto 0);
      data_reg_b : out unsigned (15 downto 0)
    );
  end component;

  component statusregister is
    port (
      clk      : in  std_logic;
      rst      : in  std_logic;
      wr_en    : in  unsigned (5 downto 0);
      data_in  : in  unsigned (5 downto 0);
      data_out : out unsigned (5 downto 0)
    );
  end component;

  component ram is
    port (
      clk      : in  std_logic;
      wr_en    : in  std_logic;
      address  : in  unsigned(7  downto 0);
      data_in  : in  unsigned(15 downto 0);
      data_out : out unsigned(15 downto 0)
    );
  end component;

  signal data_a, data_b, mux_out, ula_out, ram_out, rb_wr_data: unsigned (15 downto 0);
  signal flags_s : unsigned (5 downto 0);

begin
  ram0 : ram port map (clk => clk, wr_en => ram_wr_en, address => ula_out(7 downto 0), data_in => data_a,
    data_out => ram_out);

  mux_i : mux16bits2x1 port map (sel_data => rb_wr_src, data_in_0 => ula_out, data_in_1 => ram_out,
    data_out => rb_wr_data);

  rb : registerbank port map (clk => clk, rst => rst, wr_en => wr_en, wr_reg => wr_reg,
    rd_reg_a => rd_reg_a, rd_reg_b => rd_reg_b, wr_data => rb_wr_data, data_reg_a => data_a, 
    data_reg_b => data_b);

  alu0 : alu port map (sel_op => sel_op, data_in_a => data_a, data_in_b => mux_out, data_out => ula_out,
    flags => flags_s);

  mux_o : mux16bits2x1 port map (sel_data => alu_src, data_in_0 => data_b, data_in_1 => const, data_out => mux_out);

  sr : statusregister port map(clk => clk, rst => rst, wr_en => flags_wr_en, data_in => flags_s, 
    data_out => flags);

  data_reg_a <= data_a;

  data_reg_b <= data_b;

  data_out <= ula_out;
end architecture;
