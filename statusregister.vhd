library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity statusregister is
    port (
      clk      : in  std_logic;
      rst      : in  std_logic;
      wr_en    : in  unsigned (5 downto 0);
      data_in  : in  unsigned (5 downto 0);
      data_out : out unsigned (5 downto 0)
    );
end entity;

architecture a_statusregister of statusregister is

    component dff is
        port (
            clk      : in  std_logic;
            rst      : in  std_logic;
            wr_en    : in  std_logic;
            data_in  : in  std_logic;
            data_out : out std_logic
        );
    end component;

    signal wr0, wr1, wr2, wr3, wr4, wr5 : std_logic;
    signal in0, in1, in2, in3, in4, in5 : std_logic;
    signal out0, out1, out2, out3, out4, out5 : std_logic;

begin
    wr0 <= wr_en(0); in0 <= data_in(0); data_out(0) <= out0;
    wr1 <= wr_en(1); in1 <= data_in(1); data_out(1) <= out1;
    wr2 <= wr_en(2); in2 <= data_in(2); data_out(2) <= out2;
    wr3 <= wr_en(3); in3 <= data_in(3); data_out(3) <= out3;
    wr4 <= wr_en(4); in4 <= data_in(4); data_out(4) <= out4;
    wr5 <= wr_en(5); in5 <= data_in(5); data_out(5) <= out5;

    Cflag : dff port map(clk => clk, rst => rst, wr_en => wr0, data_in => in0, data_out => out0);
    Zflag : dff port map(clk => clk, rst => rst, wr_en => wr1, data_in => in1, data_out => out1);
    Nflag : dff port map(clk => clk, rst => rst, wr_en => wr2, data_in => in2, data_out => out2);
    Vflag : dff port map(clk => clk, rst => rst, wr_en => wr3, data_in => in3, data_out => out3);
    Sflag : dff port map(clk => clk, rst => rst, wr_en => wr4, data_in => in4, data_out => out4);
    Hflag : dff port map(clk => clk, rst => rst, wr_en => wr5, data_in => in5, data_out => out5);
end architecture ;