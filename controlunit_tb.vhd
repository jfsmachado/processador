library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity controlunit_tb is
end entity;

architecture a_controlunit_tb of controlunit_tb is
  component controlunit
    port (
      clk             : in  std_logic;
      rst             : in  std_logic;
      instruction     : in  unsigned (14 downto 0);
      sr_flags        : in  unsigned (5  downto 0);
      sr_flags_wr_en  : out unsigned (5  downto 0);
      state           : out std_logic;
      pc_wr_en        : out std_logic;
      pc_jump_en      : out std_logic;
      pc_jump_address : out unsigned (15 downto 0);
      alu_src         : out std_logic;
      alu_sel_op      : out unsigned (1  downto 0);
      rb_wr_en        : out std_logic;
      rb_wr_reg       : out unsigned (2  downto 0);
      rb_rd_reg_a     : out unsigned (2  downto 0);
      rb_rd_reg_b     : out unsigned (2  downto 0);
      alu_const       : out unsigned (15 downto 0);
      pc_branch_en    : out std_logic;
      pc_branch_delta : out unsigned (15 downto 0);
      rb_wr_src       : out std_logic;
      ram_wr_en       : out std_logic
    );
  end component;

  signal clk, rst, state, pc_wr_en, pc_jump_en, alu_src, rb_wr_en, pc_branch_en, rb_wr_src, ram_wr_en : std_logic;
  signal alu_sel_op : unsigned (1 downto 0);
  signal rb_wr_reg, rb_rd_reg_a, rb_rd_reg_b : unsigned (2 downto 0);
  signal sr_flags, sr_flags_wr_en : unsigned (5 downto 0);
  signal instruction : unsigned (14 downto 0);
  signal pc_jump_address, alu_const, pc_branch_delta : unsigned (15 downto 0);

begin
  uut: controlunit port map (
    clk             => clk,
    rst             => rst,
    instruction     => instruction,
    sr_flags        => sr_flags,
    sr_flags_wr_en  => sr_flags_wr_en,
    state           => state,
    pc_wr_en        => pc_wr_en,
    pc_jump_en      => pc_jump_en,
    pc_jump_address => pc_jump_address,
    alu_src         => alu_src,
    alu_sel_op      => alu_sel_op,
    rb_wr_en        => rb_wr_en,
    rb_wr_reg       => rb_wr_reg,
    rb_rd_reg_a     => rb_rd_reg_a,
    rb_rd_reg_b     => rb_rd_reg_b,
    alu_const       => alu_const,
    pc_branch_en    => pc_branch_en,
    pc_branch_delta => pc_branch_delta,
    rb_wr_src       => rb_wr_src,
    ram_wr_en       => ram_wr_en
  );

  process begin
    clk <= '0';
    wait for 50 ns;
    clk <= '1';
    wait for 50 ns;
  end process;

  process begin
    rst <= '1';
    wait for 50 ns;
    rst <= '0';
    wait for 50 ns;
    wait;
  end process;

  process begin
    sr_flags <= "010000";
    wait for 100 ns;
    instruction <= "111000000011101"; -- LDI R3,5
    wait for 100 ns;
    instruction <= "111000001100000"; -- LDI R4,8
    wait for 100 ns;
    instruction <= "101100000101100"; -- MOV R5,R4
    wait for 100 ns;
    instruction <= "001100000101011"; -- ADD R5,R3
    wait for 100 ns;
    instruction <= "010100000101001"; -- SUBI R5,1
    wait for 100 ns;
    instruction <= "101000000010100"; -- JMP 20
    wait for 100 ns;
    instruction <= "111000000101000"; -- LDI R5,0
    wait for 100 ns;
    instruction <= "101100000011101"; -- MOV R3,R5
    wait for 100 ns;
    instruction <= "101000000000010"; -- JMP 2
    wait for 100 ns;
    instruction <= "111000000011000"; -- LDI R3,0
    wait for 100 ns;
    instruction <= "011000000111101"; -- SUB R7,R5
    wait for 100 ns;
    instruction <= "000100000101010"; -- CP R5,R2
    wait for 100 ns;
    instruction <= "001000011011110"; -- CPI R3,30
    wait for 100 ns;
    instruction <= "110011111111100"; -- BRLT -4
    wait for 100 ns;
    sr_flags <= "101111";
    wait for 100 ns;
    instruction <= "110011111111100"; -- BRLT -4
    wait for 100 ns;
    instruction <= "100000000011111"; -- LDS R3,R7
    wait for 100 ns;
    instruction <= "100100000001100"; -- STS R1,R4
    wait for 100 ns;
    instruction <= "000000000000000";
    wait for 100 ns;
    wait;
  end process;
end architecture;
