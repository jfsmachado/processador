import os

os.system("rm -rf work-obj93.cf")

# ALU
os.system("ghdl -a alu.vhd")
os.system("ghdl -e alu")
# ALU TESTBENCH
os.system("ghdl -a alu_tb.vhd")
os.system("ghdl -e alu_tb")
os.system("ghdl -r alu_tb --wave=alu_tb.ghw")
# os.system("gtkwave alu_tb.ghw -a alu_tb.gtkw")

# Register 16 bits
os.system("ghdl -a register16bits.vhd")
os.system("ghdl -e register16bits")
# Register 16 bits TESTBENCH
os.system("ghdl -a register16bits_tb.vhd")
os.system("ghdl -e register16bits_tb")
os.system("ghdl -r register16bits_tb --stop-time=3000ns "
          "--wave=register16bits_tb.ghw")
# os.system("gtkwave register16bits_tb.ghw -a register16bits_tb.gtkw")

# FlipFlop D
os.system("ghdl -a dff.vhd")
os.system("ghdl -e dff")
# FlipFlop D TESTBENCH
os.system("ghdl -a dff_tb.vhd")
os.system("ghdl -e dff_tb")
os.system("ghdl -r dff_tb --stop-time=3000ns "
          "--wave=dff_tb.ghw")
# os.system("gtkwave dff_tb.ghw -a dff_tb.gtkw")

# Status Register
os.system("ghdl -a statusregister.vhd")
os.system("ghdl -e statusregister")
# Status Register TESTBENCH
os.system("ghdl -a statusregister_tb.vhd")
os.system("ghdl -e statusregister_tb")
os.system("ghdl -r statusregister_tb --stop-time=3000ns "
          "--wave=statusregister_tb.ghw")
# os.system("gtkwave statusregister_tb.ghw -a statusregister_tb.gtkw ")

# Mux 16 bits 8 x 1
os.system("ghdl -a mux16bits8x1.vhd")
os.system("ghdl -e mux16bits8x1")
# Mux 16 bits 8 x 1 TESTBENCH
os.system("ghdl -a mux16bits8x1_tb.vhd")
os.system("ghdl -e mux16bits8x1_tb")
os.system("ghdl -r mux16bits8x1_tb --wave=mux16bits8x1_tb.ghw")
# os.system("gtkwave mux16bits8x1_tb.ghw -a mux16bits8x1_tb.gtkw")

# Demux 1 bit 1 x 8
os.system("ghdl -a dmx1bit1x8.vhd")
os.system("ghdl -e dmx1bit1x8")
# Demux 1 bit 1 x 8 TESTBENCH
os.system("ghdl -a dmx1bit1x8_tb.vhd")
os.system("ghdl -e dmx1bit1x8_tb")
os.system("ghdl -r dmx1bit1x8_tb --wave=dmx1bit1x8_tb.ghw")
# os.system("gtkwave dmx1bit1x8_tb.ghw -a dmx1bit1x8_tb.gtkw")

# Register Bank
os.system("ghdl -a registerbank.vhd")
os.system("ghdl -e registerbank")
# Register Bank TESTBENCH
os.system("ghdl -a registerbank_tb.vhd")
os.system("ghdl -e registerbank_tb")
os.system("ghdl -r registerbank_tb --stop-time=3000ns "
          "--wave=registerbank_tb.ghw")
# os.system("gtkwave registerbank_tb.ghw -a registerbank_tb.gtkw")

# Mux 16 bits 2 x 1
os.system("ghdl -a mux16bits2x1.vhd")
os.system("ghdl -e mux16bits2x1")
# Mux 16 bits 2 x 1 TESTBENCH
os.system("ghdl -a mux16bits2x1_tb.vhd")
os.system("ghdl -e mux16bits2x1_tb")
os.system("ghdl -r mux16bits2x1_tb --wave=mux16bits2x1_tb.ghw")
# os.system("gtkwave mux16bits2x1_tb.ghw -a mux16bits2x1_tb.gtkw")

# RAM
os.system("ghdl -a ram.vhd")
os.system("ghdl -e ram")
# RAM + TESTBENCH
os.system("ghdl -a ram_tb.vhd")
os.system("ghdl -e ram_tb")
os.system("ghdl -r ram_tb --stop-time=3000ns "
          "--wave=ram_tb.ghw")
# os.system("gtkwave ram_tb.ghw -a ram_tb.gtkw")

# ALU + Register Bank + RAM
os.system("ghdl -a alu_registerbank_ram.vhd")
os.system("ghdl -e alu_registerbank_ram")
# ALU + Register Bank TESTBENCH
os.system("ghdl -a alu_registerbank_ram_tb.vhd")
os.system("ghdl -e alu_registerbank_ram_tb")
os.system("ghdl -r alu_registerbank_ram_tb --stop-time=3500ns "
          "--wave=alu_registerbank_ram_tb.ghw")
# os.system("gtkwave alu_registerbank_ram_tb.ghw -a alu_registerbank_ram_tb.gtkw")

# ROM
os.system("ghdl -a rom.vhd")
os.system("ghdl -e rom")
# ROM TESTBENCH
os.system("ghdl -a rom_tb.vhd")
os.system("ghdl -e rom_tb")
os.system("ghdl -r rom_tb --stop-time=3000ns "
          "--wave=rom_tb.ghw")
# os.system("gtkwave rom_tb.ghw -a rom_tb.gtkw")

# PC
os.system("ghdl -a pc.vhd")
os.system("ghdl -e pc")
# PC TESTBENCH
os.system("ghdl -a pc_tb.vhd")
os.system("ghdl -e pc_tb")
os.system("ghdl -r pc_tb --stop-time=3000ns "
          "--wave=pc_tb.ghw")
# os.system("gtkwave pc_tb.ghw -a pc_tb.gtkw")

# State Machine
os.system("ghdl -a statemachine.vhd")
os.system("ghdl -e statemachine")
# State Machine TESTBENCH
os.system("ghdl -a statemachine_tb.vhd")
os.system("ghdl -e statemachine_tb")
os.system("ghdl -r statemachine_tb --stop-time=3000ns "
          "--wave=statemachine_tb.ghw")
# os.system("gtkwave statemachine_tb.ghw -a statemachine_tb.gtkw")

# PC + ROM
os.system("ghdl -a pc_rom.vhd")
os.system("ghdl -e pc_rom")
# State Machine TESTBENCH
os.system("ghdl -a pc_rom_tb.vhd")
os.system("ghdl -e pc_rom_tb")
os.system("ghdl -r pc_rom_tb --stop-time=3000ns "
          "--wave=pc_rom_tb.ghw")
# os.system("gtkwave pc_rom_tb.ghw -a pc_rom_tb.gtkw")

# Control Unit
os.system("ghdl -a controlunit.vhd")
os.system("ghdl -e controlunit")
# Control Unit TESTBENCH
os.system("ghdl -a controlunit_tb.vhd")
os.system("ghdl -e controlunit_tb")
os.system("ghdl -r controlunit_tb --stop-time=3000ns "
          "--wave=controlunit_tb.ghw")
# os.system("gtkwave controlunit_tb.ghw -a controlunit_tb.gtkw")


# Processador
os.system("ghdl -a processador.vhd")
os.system("ghdl -e processador")

# Processador TESTBENCH
os.system("ghdl -a processador_tb.vhd")
os.system("ghdl -e processador_tb")
os.system("ghdl -r processador_tb --stop-time=494000ns "
          "--wave=processador_tb.ghw")
os.system("gtkwave processador_tb.ghw -a processador_tb.gtkw")
