library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ram_tb is
end entity;

architecture a_ram_tb of ram_tb is
  component ram
    port (
        clk      : in  std_logic;
        wr_en    : in  std_logic;
        address  : in  unsigned(7  downto 0);
        data_in  : in  unsigned(15 downto 0);
        data_out : out unsigned(15 downto 0)
    );
  end component;

  signal clk, wr_en : std_logic;
  signal address : unsigned (7 downto 0);
  signal data_in, data_out : unsigned (15 downto 0);

begin
  uut: ram port map (
    clk      => clk,
    wr_en    => wr_en,
    address  => address,
    data_in  => data_in,
    data_out => data_out 
  );

  process begin
    clk <= '0';
    wait for 50 ns;
    clk <= '1';
    wait for 50 ns;
  end process;

  process begin
    wr_en <= '0';
    address <= "00000000";
    data_in <= "0000000000000000";
    wait for 100 ns;
    address <= "00000000";
    data_in <= "1111111111111111";
    wait for 100 ns;
    wr_en <= '1';
    address <= "01010100";
    data_in <= "0000101100100011";
    wait for 100 ns;
    address <= "00010100";
    data_in <= "1001011001011000";
    wait for 100 ns;
    address <= "01101100";
    data_in <= "1110000001111111";
    wait for 100 ns;
    address <= "01010010";
    data_in <= "1100000101110010";
    wait for 100 ns;
    address <= "00111011";
    data_in <= "1001100100111000";
    wait for 100 ns;
    address <= "00101101";
    data_in <= "1111110100100011";
    wait for 100 ns;
    address <= "01000100";
    data_in <= "1001111100110101";
    wait for 100 ns;
    address <= "00011000";
    data_in <= "1101000001101100";
    wait for 100 ns;
    wr_en <= '0';
    address <= "01010100";
    wait for 100 ns;
    address <= "00010100";
    wait for 100 ns;
    address <= "01101100";
    wait for 100 ns;
    address <= "01010010";
    wait for 100 ns;
    address <= "00111011";
    wait for 100 ns;
    address <= "00101101";
    wait for 100 ns;
    address <= "01000100";
    wait for 100 ns;
    address <= "00011000";
    wait for 100 ns;
    wait;
  end process;
end architecture;
