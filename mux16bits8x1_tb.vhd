library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux16bits8x1_tb is
end entity;

architecture a_mux16bits8x1_tb of mux16bits8x1_tb is
  component mux16bits8x1
    port (
      sel_data  : in  unsigned (2  downto 0);
      data_in_0 : in  unsigned (15 downto 0);
      data_in_1 : in  unsigned (15 downto 0);
      data_in_2 : in  unsigned (15 downto 0);
      data_in_3 : in  unsigned (15 downto 0);
      data_in_4 : in  unsigned (15 downto 0);
      data_in_5 : in  unsigned (15 downto 0);
      data_in_6 : in  unsigned (15 downto 0);
      data_in_7 : in  unsigned (15 downto 0);
      data_out  : out unsigned (15 downto 0)
    );
  end component;

  signal sel_data : unsigned (2  downto 0);
  signal data_in_0, data_in_1, data_in_2, data_in_3, data_in_4, data_in_5, data_in_6, data_in_7, data_out : unsigned (15 downto 0);

begin
  uut: mux16bits8x1 port map (
    sel_data  => sel_data,
    data_in_0 => data_in_0,
    data_in_1 => data_in_1,
    data_in_2 => data_in_2,
    data_in_3 => data_in_3,
    data_in_4 => data_in_4,
    data_in_5 => data_in_5,
    data_in_6 => data_in_6,
    data_in_7 => data_in_7,
    data_out  => data_out
  );

  process begin
    data_in_0 <= "0000000000000000";
    data_in_1 <= "0000000000000001";
    data_in_2 <= "0000000000000010";
    data_in_3 <= "0000000000000011";
    data_in_4 <= "0000000000000100";
    data_in_5 <= "0000000000000101";
    data_in_6 <= "0000000000000110";
    data_in_7 <= "0000000000000111";
    wait for 50 ns;
    sel_data <= "000";
    wait for 50 ns;
    sel_data <= "001";
    wait for 50 ns;
    sel_data <= "010";
    wait for 50 ns;
    sel_data <= "011";
    wait for 50 ns;
    sel_data <= "100";
    wait for 50 ns;
    sel_data <= "101";
    wait for 50 ns;
    sel_data <= "110";
    wait for 50 ns;
    sel_data <= "111";
    wait for 50 ns;
    wait;
  end process;
end architecture;
