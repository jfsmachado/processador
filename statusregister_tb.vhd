library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity statusregister_tb is
end entity;

architecture a_statusregister_tb of statusregister_tb is
  component statusregister
    port (
        clk      : in  std_logic;
        rst      : in  std_logic;
        wr_en    : in  unsigned (5 downto 0);
        data_in  : in  unsigned (5 downto 0);
        data_out : out unsigned (5 downto 0)
    );
  end component;

  signal clk, rst : std_logic;
  signal wr_en, data_in, data_out : unsigned (5 downto 0);

begin
  uut: statusregister port map (
    clk      => clk,
    rst      => rst,
    wr_en    => wr_en,
    data_in  => data_in,
    data_out => data_out
  );

  process begin
    clk <= '0';
    wait for 50 ns;
    clk <= '1';
    wait for 50 ns;
  end process;

  process begin
    rst <= '1';
    wait for 100 ns;
    rst <= '0';
    wait for 500 ns;
    rst <= '1';
    wait for 100 ns;
    rst <= '0';
    wait;
  end process;

  process begin
    wr_en <= "000000";
    data_in <= "111111";
    wait for 100 ns;
    wr_en <= "101010";
    data_in <= "010101";
    wait for 100 ns;
    wr_en <= "010101";
    data_in <= "101010";
    wait for 100 ns;
    wr_en <= "010101";
    data_in <= "010101";
    wait for 100 ns;
    wr_en <= "101010";
    data_in <= "101010";
    wait for 100 ns;
    wr_en <= "101010";
    data_in <= "000000";
    wait for 100 ns;
    wr_en <= "010101";
    data_in <= "000000";
    wait for 100 ns;
    wr_en <= "111111";
    data_in <= "111111";
    wait for 100 ns;
    wr_en <= "111111";
    data_in <= "000000";
    wait for 100 ns;
    wait;
  end process;
end architecture;
