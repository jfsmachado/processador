library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity registerbank is
  port (
    clk        : in  std_logic;
    rst        : in  std_logic;
    wr_en      : in  std_logic;
    wr_reg     : in  unsigned (2  downto 0);
    rd_reg_a   : in  unsigned (2  downto 0);
    rd_reg_b   : in  unsigned (2  downto 0);
    wr_data    : in  unsigned (15 downto 0);
    data_reg_a : out unsigned (15 downto 0);
		data_reg_b : out unsigned (15 downto 0)
  );
end entity;

architecture a_registerbank of registerbank is

  component register16bits is
    port (
      clk      : in  std_logic;
      rst      : in  std_logic;
      wr_en    : in  std_logic;
      data_in  : in  unsigned (15 downto 0);
      data_out : out unsigned (15 downto 0)
    );
  end component;

  component mux16bits8x1 is
    port (
      sel_data  : in  unsigned (2  downto 0);
      data_in_0 : in  unsigned (15 downto 0);
      data_in_1 : in  unsigned (15 downto 0);
      data_in_2 : in  unsigned (15 downto 0);
      data_in_3 : in  unsigned (15 downto 0);
      data_in_4 : in  unsigned (15 downto 0);
      data_in_5 : in  unsigned (15 downto 0);
      data_in_6 : in  unsigned (15 downto 0);
      data_in_7 : in  unsigned (15 downto 0);
      data_out  : out unsigned (15 downto 0)
    );
  end component;

  component dmx1bit1x8 is
    port (
      sel_data   : in  unsigned (2  downto 0);
      data_in    : in  std_logic;
      data_out_0 : out std_logic;
      data_out_1 : out std_logic;
      data_out_2 : out std_logic;
      data_out_3 : out std_logic;
      data_out_4 : out std_logic;
      data_out_5 : out std_logic;
      data_out_6 : out std_logic;
      data_out_7 : out std_logic
    );
  end component;

  signal en_0, en_1, en_2, en_3, en_4, en_5, en_6, en_7 : std_logic;
  signal out_0, out_1, out_2, out_3, out_4, out_5, out_6, out_7 : unsigned (15 downto 0);

begin
  reg_0 : register16bits port map(clk => clk, rst => rst, wr_en => en_0, data_in => wr_data, data_out => out_0);
  reg_1 : register16bits port map(clk => clk, rst => rst, wr_en => en_1, data_in => wr_data, data_out => out_1);
  reg_2 : register16bits port map(clk => clk, rst => rst, wr_en => en_2, data_in => wr_data, data_out => out_2);
  reg_3 : register16bits port map(clk => clk, rst => rst, wr_en => en_3, data_in => wr_data, data_out => out_3);
  reg_4 : register16bits port map(clk => clk, rst => rst, wr_en => en_4, data_in => wr_data, data_out => out_4);
  reg_5 : register16bits port map(clk => clk, rst => rst, wr_en => en_5, data_in => wr_data, data_out => out_5);
  reg_6 : register16bits port map(clk => clk, rst => rst, wr_en => en_6, data_in => wr_data, data_out => out_6);
  reg_7 : register16bits port map(clk => clk, rst => rst, wr_en => en_7, data_in => wr_data, data_out => out_7);

  dmx_0 : dmx1bit1x8 port map(sel_data => wr_reg, data_in => wr_en, data_out_0 => en_0, data_out_1 => en_1,
        data_out_2 => en_2, data_out_3 => en_3, data_out_4 => en_4, data_out_5 => en_5, data_out_6 => en_6, data_out_7 => en_7);

  mux_a : mux16bits8x1 port map(sel_data => rd_reg_a, data_in_0 => out_0, data_in_1 => out_1, data_in_2 => out_2,
     data_in_3 => out_3, data_in_4 => out_4, data_in_5 => out_5, data_in_6 => out_6, data_in_7 => out_7, data_out => data_reg_a);

  mux_b : mux16bits8x1 port map(sel_data => rd_reg_b, data_in_0 => out_0, data_in_1 => out_1, data_in_2 => out_2,
    data_in_3 => out_3, data_in_4 => out_4, data_in_5 => out_5, data_in_6 => out_6, data_in_7 => out_7, data_out => data_reg_b);
end architecture;
