library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dmx1bit1x8_tb is
end entity;

architecture a_dmx1bit1x8_tb of dmx1bit1x8_tb is
  component dmx1bit1x8
    port (
      sel_data   : in  unsigned (2  downto 0);
      data_in    : in  std_logic;
      data_out_0 : out std_logic;
      data_out_1 : out std_logic;
      data_out_2 : out std_logic;
      data_out_3 : out std_logic;
      data_out_4 : out std_logic;
      data_out_5 : out std_logic;
      data_out_6 : out std_logic;
      data_out_7 : out std_logic
    );
  end component;

  signal sel_data : unsigned (2  downto 0);
  signal data_in, data_out_0, data_out_1, data_out_2, data_out_3, data_out_4, data_out_5, data_out_6, data_out_7: std_logic;

begin
  uut:  dmx1bit1x8 port map (
    sel_data   => sel_data,
    data_in    => data_in,
    data_out_0 => data_out_0,
    data_out_1 => data_out_1,
    data_out_2 => data_out_2,
    data_out_3 => data_out_3,
    data_out_4 => data_out_4,
    data_out_5 => data_out_5,
    data_out_6 => data_out_6,
    data_out_7 => data_out_7
  );

    process begin
      data_in <= '0';
      wait for 50 ns;
      sel_data <= "000";
      wait for 50 ns;
      sel_data <= "001";
      wait for 50 ns;
      sel_data <= "010";
      wait for 50 ns;
      sel_data <= "011";
      wait for 50 ns;
      sel_data <= "100";
      wait for 50 ns;
      sel_data <= "101";
      wait for 50 ns;
      sel_data <= "110";
      wait for 50 ns;
      sel_data <= "111";
      wait for 50 ns;
      data_in <= '1';
      wait for 50 ns;
      sel_data <= "000";
      wait for 50 ns;
      sel_data <= "001";
      wait for 50 ns;
      sel_data <= "010";
      wait for 50 ns;
      sel_data <= "011";
      wait for 50 ns;
      sel_data <= "100";
      wait for 50 ns;
      sel_data <= "101";
      wait for 50 ns;
      sel_data <= "110";
      wait for 50 ns;
      sel_data <= "111";
      wait for 50 ns;
      wait;
    end process;
  end architecture;
